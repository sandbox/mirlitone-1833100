��    %      D  5   l      @     A     U     b     w     �  4   �  	   �     �     �     �     �          "     1     ?  7   R     �     �     �     �     �     �     �     
  
     =   #     a  Q   ~  N   �  �       �     �  .   �  3        8     M  P  b     �	     �	  '   �	     
     
  ;   #
  	   _
  '   i
     �
     �
     �
     �
  
   �
     �
     �
  G        M     `  '   x     �     �     �     �     �       E   )  '   o  V   �  
   �  �  �     �     �  &   	  G   0  %   x  ,   �               !       %                    #             	                                          $                                            
                       "               @level in @activity @user skills Any user skill block Become our expert! Beginner Concerning this particular skill, you would say that Confirmed Currently connected User skill Expert I can  I can learn I have some experience I'm a beginner I'm an expert I'm not interested I'm willing to help someone with this, if i'm available Know Nothing in Knowledge items Latent competency chooser Level of competency List of Knowledge List of skills Meet our experts Save settings See offers Show the level of competencies for all people on the platform The settings have been saved The vocabulary that describes the competencies you may have on a particular topic This field tells the level of competency for a specific competence for a user. Volunteer offer enabler allow user to describe with no pains the skills he can bring to the community. Community sites we are targetting usually allow user to post classified ad backed by a taxonomy consisting of the various skills a user can bring. However, it could be a pain to list everything a user knows/can do and we cannot really asset if he's an expert or a beginer. This module will handle those issues Your skills default view pattern used for the link that displays offers please add a field in user profile to handle skills skills you can share skills you could use Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-11-29 09:09+0100
PO-Revision-Date: 2012-11-29 09:15+0100
Last-Translator: Nicolas herbaut <nicolas82@seldemars.org>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 @level en @activity Compétence de @user block des compétences des utilisateurs Devenez notre expert! Débutat Concernant cette compétence particulière, diriez vous que Confirmé Compétences de l'utilisateur connecté Expert Je peux je peux apprendre J'ai de l'expérience Je débute Je suis un expert Je ne suis suis pas intéressé Je suis d'accord pour aider quelqu'un là dessus, si je suis disponible Ne connait rien en Element de connaissance Sélectionneur de compétences latentes Niveau de compétence Liste des connaissances Liste des compétences Rencontrez nos experts! Sauvegarder les paramètres Voir les offres Montre le niveau de compétence de tous les membres sur la plateforme Les péramètres ont été sauvegardés Le vocabulaire qui décrit les compétences que vous pouvez avoir sur un sujet précis Ce champs  Volunteer offer enabler permet à un utilisateur de décrire sans difficulté les compétences qu'il peut apporter à la communauté; Les sites communautaires permettent en général  à leur utilisateur de poster des annonces liées à une taxonomie représentant les diverses compétences que l'utilisateur peut ammener. Cependant, il peut être difficile de lister tout ce qu'un utilisateur sait/peut faire ou de savoir s'il est un expert ou un débutant. Ce module gère ces problématiques Vos compétences vue par défaut Patron utilisé pour lister les offres Ajouter ce champ sur le profil utilisateur pour gérer les compétences Compétences que vous pouvez partager Compétences dont vous pourriez avoir besoin 